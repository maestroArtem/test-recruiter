<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantVacancy extends Model
{
    protected $fillable = ['applicant_id','vacancy_id','status_id','file'];

    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }

    public function vacancy()
    {
        return $this->belongsTo(Vacancy::class);
    }

    public function status()
    {
        return $this->belongsTo(ApplicantStatus::class);
    }
}
