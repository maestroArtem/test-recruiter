<?php


namespace App\Http\Controllers\Api;

use App\Applicant;
use App\ApplicantVacancy;
use App\Vacancy;
use App\VacancyStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApplicantController extends Controller
{
    public function getAllApplicants(Request $request)
    {
        $appList = ApplicantVacancy::with('applicant','vacancy')->get();
        //todo pagination

        return response()->json($appList);
    }

    public function applyToVacancy($vacancyId, Request $request)
    {
        //todo validate request
        $applicant = Applicant::updateOrCreate([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            ],[
                'description'=>$request->description,
        ]);
        //todo file upload
        $status = VacancyStatus::whereName(Vacancy::INIT_STATUS)->first();
        if($status){
            ApplicantVacancy::create([
                'applicant_id'=>$applicant->id,
                'vacancy_id'=>$vacancyId,
                'file'=>$request->file,
                'status_id'=>$status->id,
            ]);
        }

        return response('Ok');
    }
}
