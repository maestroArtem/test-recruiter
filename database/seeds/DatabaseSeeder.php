<?php

use App\{ApplicantStatus, Vacancy, VacancyStatus};
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $vacanciesList = ['Archived','Closed',Vacancy::INIT_STATUS];
        $applicantsList = ['New','In interview','Rejected','Confirmed'];
        foreach ($vacanciesList as $item) {
            VacancyStatus::updateOrCreate(['name'=>$item]);
        }
        foreach ($applicantsList as $item) {
            ApplicantStatus::updateOrCreate(['name'=>$item]);
        }
    }
}
