<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    protected $fillable = ['title','description','status_id'];
    const INIT_STATUS = 'Open';

    public function status()
    {
        return $this->belongsTo(VacancyStatus::class);
    }
    public function applicants()
    {
        return $this->belongsToMany(Applicant::class,'vacancies','id','applicant_id','zipcode_id');
    }

    public function scopeActive($query)
    {
        return $query->whereHas('status', function ($qStatus){
            $qStatus->whereName(self::INIT_STATUS);
        });
    }

}
