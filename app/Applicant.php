<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    protected $fillable = ['name','phone','email','description'];

    public function vacancies()
    {
        return $this->hasMany(ApplicantVacancy::class);
    }
}
