<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vacancy;


class VacancyController extends Controller
{
    public function getActiveVacancies(Request $request)
    {
        $vacancies = Vacancy::active()->get();
        //todo pagination
        return response()->json($vacancies);
    }
}
