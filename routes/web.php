<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin');
});
Route::get('home', function () {
    return redirect('admin');
});

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
    'confirm' => false,
]);

//Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::group(['as' => 'vacancies.'], function () {
        Route::get('/', 'VacancyController@index')->name('list');
        Route::get('/edit/{id?}', 'VacancyController@edit')->name('edit');
        Route::post('/store/{id?}', 'VacancyController@store')->name('store');
        Route::post('/vacancytitle', 'VacancyController@vacancyChangeTitle')->name('title');
    });
    Route::group(['as' => 'applicants.'], function () {
        Route::get('/applicants', 'ApplicantController@index')->name('list');
    });
});
