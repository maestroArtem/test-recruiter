@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('layouts.messages_from_session')
        </div>
        <div class="row justify-content-center shadow-sm sticky-top font-weight-bold mt-2">
            <div class="col-md-2">Date</div>
            <div class="col-md-2">Name</div>
            <div class="col-md-2">Phone</div>
            <div class="col-md-2">Email</div>
            <div class="col-md-2">Vacancy</div>
            <div class="col-md-1">Status</div>
            <div class="col-md-1">File</div>
        </div>
        @foreach($applicants as $applicant)
            <div class="row justify-content-center mt-1">
                <div class="col-md-2">{{$applicant->created_at->format('d-m-Y')}}</div>
                <div class="col-md-2">{{$applicant->applicant->name}}</div>
                <div class="col-md-2">{{$applicant->applicant->phone}}</div>
                <div class="col-md-2">{{$applicant->applicant->email}}</div>
                <div class="col-md-2">{{$applicant->vacancy->title}}</div>
                <div class="col-md-1">{{$applicant->status->name}}</div>
                <div class="col-md-1">{{$applicant->file}}</div>
            </div>
        @endforeach
        <div class="row justify-content-center">
            {{$applicants->links()}}
        </div>
    </div>
@endsection
