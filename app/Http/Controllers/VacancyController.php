<?php

namespace App\Http\Controllers;

use App\{Vacancy, VacancyStatus};
use App\Http\Requests\VacancyFormRequest;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    const VACANCIES_PER_PAGE = 15;

    public function index(Request $request)
    {
        $vacancies = Vacancy::with('status')->latest()->paginate(self::VACANCIES_PER_PAGE);

        return view('vacancy.vacancy',compact('vacancies'));
    }

    public function edit($vacancyId=0)
    {
        $vacancy = null;

        $statuses = VacancyStatus::pluck('name','id');
        if(empty($vacancyId)){
            $vacancy = new Vacancy();
            $vacancy->status_id = $statuses->search(Vacancy::INIT_STATUS);
        }else {
            $vacancy = Vacancy::findOrFail($vacancyId);
        }

        return view('vacancy.vacancy_form',compact('vacancy','statuses'));
    }

    public function store(VacancyFormRequest $request,$vacancyId=0)
    {
        if(empty($vacancyId)){
            Vacancy::create($request->all());
        }else{
            Vacancy::findOrFail($vacancyId)->update($request->all());
        }

        return redirect()->route('vacancies.list')->withMessage('Vacancy was successful added.');
    }
    public function vacancyChangeTitle(Request $request)
    {
        if(empty($request->id) || empty($request->title)){
            return response("Id or Title not found", 201);
        }
        Vacancy::findOrFail($request->id)->update(
            ['title'=>$request->title]
        );

        return response("Ok", 200);
    }
}
