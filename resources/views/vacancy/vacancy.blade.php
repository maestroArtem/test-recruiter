@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('layouts.messages_from_session')
        </div>
        <div class="row mt-2">
            <div class="col-md-5"><a class="btn btn-primary" href="{{route('vacancies.edit')}}">Add vacancy</a></div>
        </div>
        <div class="row justify-content-center shadow-sm sticky-top font-weight-bold mt-2">
            <div class="col-md-2">Date</div>
            <div class="col-md-3">Title</div>
            <div class="col-md-5">Description</div>
            <div class="col-md-1">Status</div>
            <div class="col-md-1">Actions</div>
        </div>
        @foreach($vacancies as $vacancy)
        <div class="row justify-content-center mt-1">
            <div class="col-md-2">{{$vacancy->created_at->format('d-m-Y')}}</div>
            <div class="col-md-3">
                <input-saver value="{{$vacancy->title}}" url='{{route('vacancies.title')}}' id="{{$vacancy->id}}" field="title"></input-saver>
            </div>
            <div class="col-md-5">{{$vacancy->description}}</div>
            <div class="col-md-1">{{optional($vacancy->status)->name}}</div>
            <div class="col-md-1">
                <a class="btn btn-primary" href="{{route('vacancies.edit',$vacancy->id)}}">Edit</a>
            </div>
        </div>
        @endforeach
        <div class="row justify-content-center">
            {{$vacancies->links()}}
        </div>
    </div>
@endsection
