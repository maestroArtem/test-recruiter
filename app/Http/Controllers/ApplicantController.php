<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\ApplicantVacancy;
use Illuminate\Http\Request;

class ApplicantController extends Controller
{
    const APPLICANT_PER_PAGE = 15;

    public function index(Request $request)
    {
        $applicants = ApplicantVacancy::with('applicant','vacancy')
            ->latest()->paginate(self::APPLICANT_PER_PAGE);

        return view('applicant.applicantlist',compact('applicants'));
    }
}
