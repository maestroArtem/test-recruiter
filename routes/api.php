<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('vacancies', 'Api\\VacancyController@getActiveVacancies');
Route::get('applicant_apply/{id}', 'Api\\ApplicantController@applyToVacancy');
Route::get('get_all_applicants', 'Api\\ApplicantController@getAllApplicants');
