<ul class="navbar-nav mr-auto">
    <li class="mr-3 {{Route::currentRouteName()=='vacancies.list'?'font-weight-bold':''}}">
        <a href="{{route('vacancies.list')}}">Vacancy list</a>
    </li>
    <li class="mr-3 {{Route::currentRouteName()=='applicants.list'?'font-weight-bold':''}}">
        <a href="{{route('applicants.list')}}">Applicants list</a>
    </li>
</ul>
