@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Vacancy editor</h3>
        @include('layouts.messages_from_session')
        <form action="{{route('vacancies.store',$vacancy->id)}}" method="post">
            @csrf
            <div class="col-md-12">
                <label for="name">Title *</label>
                <input name="title" value="{{$vacancy->title}}" required class="form-control">
            </div>
            <div class="col-md-12">
                <label for="name">Description *</label>
                <textarea name="description" required class="form-control">{{$vacancy->description}}</textarea>
            </div>
            <div class="col-md-12">
                <label for="name">Status *</label>
                <select name="status_id" required class="form-control">
                    @foreach($statuses as $key=>$status)
                    <option value="{{$key}}" {{$key==$vacancy->status_id?'selected':''}}>{{$status}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-12 mt-2">
                <button class="btn btn-primary" type="submit">{{empty($vacancy->id)?'Create vacancy':'Save vacancy'}}</button>
                <a class="btn btn-secondary" href="{{route('vacancies.list')}}">Back</a>
            </div>
        </form>
    </div>
@endsection
